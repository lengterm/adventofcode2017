# Advent Of Code 2017

# Taisyklės:

1. Užduočių kodą comitinam į day[diena]/[vardas]/ direktoriją
1. Kodas turi būti sucomititas į šią repozitoriją kaip įmanoma greičiau to užduoties įveikimo
1. Jeigu uždoties 1 dalies nespėjam įgyvendinti per 48h po užduoties pasirodymo teks statyti alaus

Taškus ir alų galima sekti čia: http://dev.fobas.lt:1337/#/
